[CmdletBinding()]
param(
    [string]$TestScript,
    [string]$ModuleRoot
)

Write-Verbose "Executing test '$TestScript'"
. (Join-Path $ModuleRoot poshunit.ps1)
$message = (& {
    try {
        $start = Get-Date
        (& $TestScript) 2>&1 | Out-Null
        $duration = (Get-Date) - $start
    } catch {
        $_
    }
})
$result = (-not [bool]$message)
$item = Get-Item $TestScript
New-Object PSObject -Property @{
    Name = ($item.BaseName.Substring(0, $item.BaseName.Length - 5))
    Passed = $result
    Message = $message
    Duration = $duration
}
