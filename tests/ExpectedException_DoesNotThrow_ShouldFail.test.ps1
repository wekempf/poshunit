try {
    ExpectException string { }
} catch {
    return $true
}

throw 'ExpectException block did not throw but command did not fail.'