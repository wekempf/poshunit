try {
    Assert { $false } 'xyzzy'
} catch {
    if ($_.TargetObject -ne 'xyzzy') {
        throw "Expected: 'xyzzy', Actual: $_"
    }
    return $true
}

throw 'Assert did not throw.'