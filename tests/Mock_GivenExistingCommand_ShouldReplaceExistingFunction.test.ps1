Mock Push-Location { $Path }
$result = Push-Location -Path ..
Assert { $result -eq '..' } 'Mock did not replace existing function.'