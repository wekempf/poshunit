$ModuleRoot = $PSScriptRoot

<#
.Synopsis
   Executes PoshUnit test scripts.
.DESCRIPTION
   Runs each specified test script within a new shell with -NoProfile specified.
.EXAMPLE
   PS> Invoke-UnitTest

   Description
   -----------
   Runs all of the unit test scripts found recursively in the current directory.
.EXAMPLE
   PS> Invoke-UnitTest C:\SomeDirectory

   Description
   -----------
   Runs all of the unit tst scripts found recursively in C:\SomeDirectory.
.EXAMPLE
    PS> Invoke-UnitTest C:\Script1.test.ps1,C:\Script2.test.ps1

    Description
    -----------
    Runs C:\Script1.test.ps1 and C:\Script2.test.ps2
#>
function Invoke-UnitTest
{
    [CmdletBinding()]
    Param
    (
        # The path to the script(s) to run. If the path is a directory all scripts ending in .test.ps1 found recursively within the directory are run.
        [Parameter(Mandatory=$false,
                   ValueFromPipeline=$true,
                   ValueFromPipelineByPropertyName=$true,
                   Position=0)]
        [ValidateNotNullOrEmpty()]
        [ValidateScript({ (Test-Path $_) -and (Resolve-Path $_).Provider.Name -eq 'FileSystem' })]
        [string[]]
        $Path = (Get-Location),

        # Return only the results for failed tests.
        [switch]
        $Failed
    )

    begin {
        $tests = @()
    }

    process {
        $Path | ForEach-Object {
            if (Test-Path $_ -PathType Container) {
                Get-ChildItem -Recurse -Include *.test.ps1 -Path $_ |
                    ForEach-Object { $tests += $_ }
            } else {
                $tests += Get-Item $_
            }
        }
    }

    end {
        $totalTests = $tests.Length
        $currentTest = 0
        $results = $tests | ForEach-Object {
            $testName = (Get-Item $_).BaseName
            $percent = ($currentTest / $totalTests) * 100
            Write-Progress -Activity "Running unit tests" -CurrentOperation $testName -PercentComplete $percent
            $Verbose = ''
            $Debug = ''
            if ($PSBoundParameters['Verbose']) {
                $Verbose = '-Verbose'
            }
            if ($PSBoundParameters['Debug']) {
                $Debug = '-Debug'
            }
            $command = "& { $ModuleRoot\RunTest.ps1 -TestScript:$_ -ModuleRoot:$ModuleRoot $Verbose $Debug }"
            Write-Debug $command
            PowerShell -NoLogo -NoProfile -NonInteractive -OutputFormat XML -Command $command 
            $currentTest += 1
        }
        Write-Progress -Completed -Activity 'Done.'
        $results |
            Sort-Object Passed,@{Expression='Duration';Ascending=$false} |
            Select-Object Name,Passed,Message,Duration |
            Where-Object { (-not $Failed) -or ($_.Passed -eq $false) } |
            ForEach-Object { $_.pstypenames.insert(0, 'PoshUnit.TestResult'); $_ }
    }
}
Set-Alias -Name ut -Value Invoke-UnitTest
Export-ModuleMember -Function Invoke-UnitTest -Alias ut
