function Assert {
    param(
        [ScriptBlock]$Script,
        [string]$Message = $null
    )
    $result = & $Script
    if (-not $result) {
        if ($Message) {
            throw $Message
        } else {
            throw "Assertion '$($Script.ToString().Trim())' failed."
        }
    }
    $true
}

function ExpectException {
    param(
        [string]$TypeName,
        [ScriptBlock]$Script
    )
    $found = $false
    try {
        & $Script
    }
    catch {
        Write-Verbose "ExpectException caught: $($_.Exception)"
        $exceptionType = [Type]$TypeName
        if ($_.Exception.GetType() -eq [System.Management.Automation.RuntimeException]) {
            $actualType = $_.TargetObject.GetType()
        } else {
            $actualType = $_.Exception.GetType()
        }
        if ($exceptionType.IsAssignableFrom($actualType)) {
            $found = $true
        } else {
            throw "Expected exception: '$exceptionType', Actual exception: '$actualType'"
        }
    }
    if (-not $found) {
        throw "Expected exception: '$exceptionType', Actual exception: [none]"
    }
}

function Mock {
    param(
        [string]$Name,
        [ScriptBlock]$Script
    )
    $command = Get-Command $Name
    if (-not $command) {
        throw 'Cannot mock a command that does not exist'
    }
    $metadata = New-Object System.Management.Automation.CommandMetadata $command
    'Verbose','Debug','ErrorAction','WarningAction','ErrorVariable','WarningVariable','OutVariable','OutBuffer' |
        ForEach-Object {
            $metadata.Parameters.Remove($_) | Out-Null
        }
    $binding = [Management.Automation.ProxyCommand]::GetCmdletBindingAttribute($metadata)
    $params = [Management.Automation.ProxyCommand]::GetParamBlock($metadata)
    $newContent = $Script.ToString()
    Set-Item Function:\script:$Name -value "$binding `r`n param ( $params )Process{ `r`n$newContent}"
}
